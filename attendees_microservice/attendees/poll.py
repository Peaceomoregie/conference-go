import json
import requests

from .models import ConferenceVO


def get_conferences():
    """
    This sends a request to the monolith microservice at the GET url
    which will send back JSON data. We use json.loads to convert the JSON
    data to a Python dictionary that we can utilize
    """
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)

    """
    For each conference data object, we will compare the "href" value to
    see if it already exists in conferenceVO. The function will then either
    update or create a conferenceVO instance with the information set by defaults.
    """
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
