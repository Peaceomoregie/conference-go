import requests, json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def get_picture(city):
    url = "https://api.pexels.com/v1/search?query=%s&per_page=1" % city
    headers = {'Authorization': PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    parsed_r = json.loads(r.text)
    return parsed_r["photos"][0]["url"]


def get_weather(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct?q=%s,%s&limit=1&appid=%s" % (city, state, OPEN_WEATHER_API_KEY)
    geo_headers = {'Authorization': OPEN_WEATHER_API_KEY}
    geo_r = requests.get(geo_url, headers=geo_headers)
    geo_parsed_r = json.loads(geo_r.text)
    lat, lon = geo_parsed_r[0]["lat"], geo_parsed_r[0]["lon"]
    weather_url = "https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s" % (lat, lon, OPEN_WEATHER_API_KEY)
    weather_header = {'Authorization': OPEN_WEATHER_API_KEY}
    weather_r = requests.get(weather_url, headers=weather_header)
    weather_parsed_r = json.loads(weather_r.text)
    temp = str(round(((weather_parsed_r["main"]["temp"] - 273.15) * 9/5 + 32), 2)) + "°F"
    description = weather_parsed_r["weather"][0]["main"] + ", " + weather_parsed_r["weather"][0]["description"]
    weather = {
        "temp": temp,
        "description": description
    }
    return weather
